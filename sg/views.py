#-*- encoding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponseRedirect,HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.utils import feedgenerator
from django.views.generic import TemplateView

from send_signal import send_signal
import random
import datetime
from models import *

def priorityName(id):
    id = int(id)
    if id==0: return u"Информационные"
    if id==1: return u"Важные"
    if id==2: return u"Критические"
    return u"Неопознано"

def getDatePeriod(tp):
    tp = int(tp)
    n = datetime.datetime.now()
    nd = datetime.datetime(n.year,n.month,n.day)
    if tp==1: # сегодня
        return datetime.datetime(n.year,n.month,n.day),n
    if tp==2: # вчера
        xn = nd - datetime.timedelta(hours=24)
        return xn,datetime.datetime(n.year,n.month,n.day)
    if tp==3: # неделя
        return n-datetime.timedelta(days=7),n
    if tp==4: # месяц
        xn = n-datetime.timedelta(days=30)
        return xn,n
    if tp==5: # год
        xn = n-datetime.timedelta(days=365)
        return xn,n
    return datetime.datetime(n.year,n.month,n.day),n
        
def home(request,template_name="index.html"):
    hide_left_menu = True
    if request.user.is_authenticated():
        return HttpResponseRedirect("/profile/")
    return render(request,template_name,locals())

def showIcons(request,template_name="icons.html"):
    return render(request,template_name,locals())

def getGuid():
    chars=['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','x','y','z']
    text = ""    
    for i in xrange(20):
        text += random.choice(chars)
    if UserProfile.objects.filter(guid=text).count()>0: return getGuid()
    return text
    
    
def registerUser(request,template_name="register.html"):
    hide_left_menu = True
    if request.method=="POST":
        username = request.POST.get("username")
        email    = request.POST.get("email","no@no.ru")        
        password = request.POST.get("password")        
        ref      = request.POST.get("ref","/")
        if ref=="" or "/login/" in ref or "/register" in ref or ref=="None" or ref==None: ref="/"
        if User.objects.filter(username__iexact=username).count()>0:
            msg=u"Имя пользователя уже занято. Выберите другой имя пользователя."
            return render(request,template_name,locals())
        if password=="": 
            msg=u"Введите пароль"
            return render(request,template_name,locals())            
        luser = User.objects.create_user(username=username,email=email,password=password,last_login=datetime.datetime.now())
        
        if luser==None: 
            msg=u"Имя пользователя уже занято"
            return render(request,template_name,locals())
        luser = authenticate(username=username,password=password)
        if luser==None: 
            msg=u"Неверное имя пользователя или пароль"
            return render(request,template_name,locals())        
        login(request,luser)
        if ref==None: ref="/"        
        UserProfile(user=luser,guid=getGuid()).save()
        
        send_signal(2,u"Зарегистрировался пользователь %s"%(username))
        return HttpResponseRedirect(ref)
    else:
        if request.user.is_anonymous(): 
            ref = request.GET.get("next")
            if ref==None:
                ref = request.META.get("HTTP_REFERER")
            is_master = request.GET.get("master")
            return render(request,template_name,locals())
        logout(request)
        return HttpResponseRedirect(request.META.get("HTTP_REFERER"))     
    
def loginUser(request,template_name="login.html"):
    hide_left_menu = True
    redirect_to = request.REQUEST.get('next', '/')

    if request.method=="POST":
        username = request.POST.get("username")
        password = request.POST.get("password")
        ref      = request.POST.get("ref","/administrator/")
        if ref==None or ref=="" or "/login" in ref: ref="/"
        luser = authenticate(username=username, password=password)
        if luser==None: 
            msg=u"Неверное имя пользователя или пароль"
            return render(request,template_name,locals())
        login(request,luser)
        if ref=="/" and redirect_to!="/": ref = redirect_to
        send_signal(1,u"Вошёл пользователь %s"%(username))
        return HttpResponseRedirect(ref)
    else:
        if request.user.is_anonymous(): 
            ref = request.GET.get("next")
            if ref==None:
                ref = request.META.get("HTTP_REFERER")
            return render(request,template_name,locals())
        
        return HttpResponseRedirect(request.META.get("HTTP_REFERER")) 

@login_required
def logoutUser(request):
    logout(request)
    next = request.GET.get("next","/")
    return HttpResponseRedirect(next)

@login_required
def profileUser(request,template_name="profile.html"):
    last_messages = MessageHistory.objects.filter(user=request.user).exclude(status=2).order_by("-id")[:20]
    dn = datetime.datetime.now()
    total_count  = MessageHistory.objects.filter(user=request.user,dt_add__gte=dn-datetime.timedelta(hours=24)).exclude(status=2).count()
    total0_count = MessageHistory.objects.filter(user=request.user,priority=0,dt_add__gte=dn-datetime.timedelta(hours=24)).exclude(status=2).count()
    total1_count = MessageHistory.objects.filter(user=request.user,priority=1,dt_add__gte=dn-datetime.timedelta(hours=24)).exclude(status=2).count()
    total2_count = MessageHistory.objects.filter(user=request.user,priority=2,dt_add__gte=dn-datetime.timedelta(hours=24)).exclude(status=2).count()
    if UserProfile.objects.filter(user=request.user).count()==0:
        UserProfile(user=request.user,guid=getGuid()).save()
    else:
        up = UserProfile.objects.filter(user=request.user)[0]
        if up.guid==None or up.guid=="":
            up.guid==getGuid()
            up.save()
    
    sources = [d.mfrom for d in MessageLast.objects.filter(user=request.user).order_by("-dt")]
    return render(request,template_name,locals())

def checkFroms(user):
    for d in MessageLast.objects.filter(user=user):
        mh = MessageHistory.objects.filter(user=user,mfrom=d.mfrom).exclude(status=2)
        if mh.count()==0: 
            d.delete()
            Message.objects.filter(user=user,mfrom=d.mfrom).delete()

@login_required
def messageFilter(request,template_name="messages.html"):
    messages = MessageHistory.objects.filter(user=request.user).exclude(status=2)
    mfrom = request.GET.get("source")
    priority = request.GET.get("priority")
    page = request.GET.get("page",1)
    header = ""
    flink = ""
    if mfrom!=None: 
        header += u"Источник: %s "%(mfrom)
        flink = "&source=%s"%mfrom
        messages = messages.filter(mfrom__exact=mfrom)
    
    if priority!=None:
        header += u"Важность: %s "%(priorityName(priority))
        flink += "&priority=%s"%priority
        messages = messages.filter(priority=priority)
        
    if request.GET.get("text","").strip()!="":
        text = request.GET.get("text","")
        header += u"Поисковые слова: %s "%(text)
        messages = messages.filter(text__icontains=text)
        
    if request.GET.get("tdt","").strip()!="":
        tdt = request.GET.get("tdt","")
        dt_start,dt_end = getDatePeriod(tdt)
        header += u"Дата: %s - %s"%(dt_start.strftime("%d.%m.%Y %H:%M"),dt_end.strftime("%d.%m.%Y %H:%M"))
        messages = messages.filter(dt_add__gte=dt_start,dt_add__lte=dt_end)
    
    m_count = messages.count()

    if request.GET.get("rm")=="1":
        messages.update(status=2)
        checkFroms(request.user)
        
    
    paginator = Paginator(messages,20)
    page = request.GET.get('page')
    try:
        messages = paginator.page(page)
    except PageNotAnInteger:
        page = 1
        messages = paginator.page(1)
    except EmptyPage:
        page = paginator.num_pages
        messages = paginator.page(paginator.num_pages)
        
    page_range = messages.paginator.page_range
    if len(page_range)>20:
        sp = 0
        if int(page)>2: sp = int(page)-2
        page_range1 = messages.paginator.page_range[sp:sp+6]
        page_range2 = messages.paginator.page_range[-6:]
        if sp+10+10>=len(page_range):
            page_range1 = messages.paginator.page_range[sp:sp+6]
            page_range2 = []
    
    
    
    sources = [d.mfrom for d in MessageLast.objects.filter(user=request.user).order_by("-dt")]
    page = int(page)
    return render(request,template_name,locals())
    
@login_required
def removeMessage(request,id):
    MessageHistory.objects.filter(pk=id,user=request.user).update(status=2)
    checkFroms(request.user)
    return HttpResponse("ok")
    
def support(request,template_name="support.html"):
    
    if request.method=="POST":
        msg="thanks"
        name = request.POST.get("name")
        if request.user.is_authenticated():
            name  = request.user.username
        email = request.POST.get('email')
        text  = request.POST.get('text')
        
        send_signal(2,u"Письмо в поддержку от %s - %s - %s"%(name,email,text))
        
    return render(request,template_name,locals())
    
        
    
    
    

def rss(request):
    guid = request.GET.get("g","")
    
    feed_url = '/rss/?guid='+guid
    f = feedgenerator.Rss201rev2Feed(
        title = u'mysignals.ru',
        link = 'http://mysignals.ru',
        description = '',
        language = 'ru',
        feed_url = feed_url
    )
    
    
    for m in MessageHistory.objects.filter(guid=guid,status=0)[:50]:
        f.add_item(title=m.mfrom+': '+m.text,
            link='http://mysignals.ru',
            pubdate=m.dt_add,
            description=m.text)
    
    return HttpResponse(f.writeString('UTF-8'))
    """
    fname = os.path.join(settings.BASE_DIR,'static/rss/%s.xml'%(guid))
    fx = open(fname,"wb")
    fx.write(f.writeString('UTF-8'))
    fx.close()

    
    return HttpResponseRedirect("/static/rss/%d.xml"%(rss.id))
    """
    
class TelegramView(TemplateView):
    template_name = 'telegram.html'
    def get_context_data(self, **kwargs):
        context = super(TelegramView, self).get_context_data(**kwargs)
        profile = UserProfile.objects.get(user=self.request.user)
        if self.request.GET.get("good")=="1" and profile.telegram_chat_id=='':
            context['msg']=u"Имя сохранено, для подтверждения имени отправьте сообщение боту и ждите ответного сообщения"
        context['is_telegram'] = profile.telegram_chat_id.strip()!=''
        context['profile']     = profile
        
        return context
        
    def post(self,request,*args,**kwargs):
        up = UserProfile.objects.get(user=request.user)
        up.telegram_name    = request.POST.get("tname")
        up.telegram_chat_id = ''
        if Telegram.objects.filter(username=up.telegram_name).exists():
            up.telegram_chat_id=Telegram.objects.filter(username=up.telegram_name)[0].chat_id
        up.save()
        return HttpResponseRedirect(reverse('telegram')+"?good=1")
        
    