#-*- encoding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.contrib import admin




class UserProfile(models.Model):
    user = models.OneToOneField(User,unique=True,verbose_name=u"Пользователь")
    guid = models.CharField(max_length=255)
    telegram_name    = models.CharField(max_length=255,null=True,blank=True,verbose_name=u"Имя в Telegram")
    telegram_chat_id = models.CharField(max_length=255,default='',blank=True,verbose_name=u"Идентификатор чата в Telegram")
    def __unicode__(self):
        return self.user.username
        
class Message(models.Model):
    user      = models.ForeignKey(User,verbose_name=u"Пользователь")
    guid      = models.CharField(max_length=255)
    mfrom     = models.CharField(max_length=255)
    priority  = models.IntegerField(default=0)
    text      = models.CharField(max_length=2048)
    dt_add    = models.DateTimeField(auto_now_add=True)
    dt_read   = models.DateTimeField(null=True,blank=True)
    status    = models.IntegerField(default=0)
    
    class Meta:
        ordering = ["-id"]
        
class MessageHistory(models.Model):
    user      = models.ForeignKey(User,verbose_name=u"Пользователь")
    guid      = models.CharField(max_length=255)
    mfrom     = models.CharField(max_length=255)
    priority  = models.IntegerField(default=0)
    text      = models.CharField(max_length=2048)
    dt_add    = models.DateTimeField(auto_now_add=True)
    dt_read   = models.DateTimeField(null=True,blank=True)
    status    = models.IntegerField(default=0)
    telegram  = models.BooleanField(default=False,verbose_name=u"Отправлять в Telegram")
    telegram_sended = models.BooleanField(default=False,verbose_name=u"Отправлено в Telegram")
    def get_priority_class(self):
        if self.priority==1: return "warning"
        if self.priority==2: return "danger"
        return ""
    class Meta:
        ordering = ["-id"]
        
    def fullText(self):
        return u"%s: %s"%(self.mfrom,self.text)
    
class MessageLast(models.Model):
    user  = models.ForeignKey(User,verbose_name=u"Пользователь")
    mfrom = models.CharField(max_length=255)
    text  = models.CharField(max_length=2048)
    priority = models.IntegerField(default=0)
    dt    = models.DateTimeField(auto_now=True)

class Channel(models.Model):
    name      = models.CharField(max_length=255)
    user      = models.ForeignKey(User,verbose_name=u"Пользователь")
    is_public = models.BooleanField(default=False)
    
class UserChannel(models.Model):
    user      = models.ForeignKey(User,verbose_name=u"Пользователь")
    channel   = models.ForeignKey(Channel,verbose_name=u"Канал")
    dt        = models.DateTimeField(auto_now_add=True)

class Telegram(models.Model):
    username = models.CharField(max_length=255)
    chat_id  = models.CharField(max_length=255)
    dt       = models.DateTimeField(auto_now_add=True)
    def __unicode__(self):
        return u"%s - %s"%(self.username,self.chat_id)

admin.site.register(UserProfile)
admin.site.register(Message)
admin.site.register(Telegram)
