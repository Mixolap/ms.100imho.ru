# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sg', '0004_auto_20161112_2223'),
    ]

    operations = [
        migrations.AddField(
            model_name='messagehistory',
            name='telegram',
            field=models.BooleanField(default=False, verbose_name='\u041e\u0442\u043f\u0440\u0430\u0432\u043b\u044f\u0442\u044c \u0432 Telegram'),
        ),
        migrations.AddField(
            model_name='messagehistory',
            name='telegram_sended',
            field=models.BooleanField(default=False, verbose_name='\u041e\u0442\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u043e \u0432 Telegram'),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='telegram_chat_id',
            field=models.CharField(default=b'', max_length=255),
        ),
    ]
