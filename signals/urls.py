#-*- encoding: utf-8 -*-
from django.conf.urls import patterns, include, url

from django.contrib import admin
from django.views.generic import TemplateView

admin.autodiscover()

from backs.views import BackupView
from sg.views import *

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'signals.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    #url(r'^a/(?P<id>\d+)/$', 'master.masters.masterCheckAppoint', name='master_check_appoint'),
    url(r'^$', 'sg.views.home', name='home'),
    url(r'^filter/$', 'sg.views.messageFilter', name='message_filter'),
    url(r'^remove/message/(?P<id>\d+)/$', 'sg.views.removeMessage', name='remove_message'),
    
    
    
    url(r'^api/send/$', 'sg.api.apiSend', name='apiSend'),
    url(r'^api/login/$', 'sg.api.apiLogin', name='apiLogin'),
    url(r'^api/froms/$', 'sg.api.apiFroms', name='apiFroms'),
    url(r'^api/messages/$', 'sg.api.apiMessages', name='apiMessages'),
    
    url(r'^api/backup/$', BackupView.as_view(), name='api_backup'),
    
    (r'^api.html$', TemplateView.as_view(template_name='api.html')),
    (r'^messages.html$', TemplateView.as_view(template_name='msgs.html')),
    (r'^webview.html$', TemplateView.as_view(template_name='webview.html')),
    
    url(r'^messages.html$', 'sg.views.showIcons', name='icons'),
    url(r'^web.html$', 'sg.views.showIcons', name='icons'),
    
    url(r'^icons.html$', 'sg.views.showIcons', name='icons'),
    
    url(r'^register/$', 'sg.views.registerUser', name='register'),
    url(r'^logout/$', 'sg.views.logoutUser', name='logout'),
    url(r'^login/$', 'sg.views.loginUser', name='login'),
    url(r'^support/$', 'sg.views.support', name='support'),
    url(r'^accounts/login/$', 'sg.views.loginUser', name='login'),
    url(r'^profile/$', 'sg.views.profileUser', name='profile'),
    url(r'^rss/$', 'sg.views.rss', name='rss'),
    url(r'^telegram/$', TelegramView.as_view(), name='telegram'),

    url(r'^admin/', include(admin.site.urls)),
    
    

)
