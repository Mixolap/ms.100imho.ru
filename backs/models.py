from django.db import models
from django.contrib.auth.models import User


class Backup(models.Model):
    user    = models.ForeignKey(User)
    guid    = models.CharField(max_length=255)
    dt      = models.DateTimeField(auto_now_add=True)
    size    = models.IntegerField(default=0)
    name    = models.CharField(max_length=255)
    fname   = models.CharField(max_length=255)
    is_last = models.BooleanField(default=True)
    