#-*- encoding: utf-8 -*-
import urllib2
import urllib

API_URL = "http://signal.ftpface.ru/api/send/?"

def send_signal(priority,text,mfrom='test',guid='jjxkc6fv1zud3fucb35q'):
    headers = { 
        'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Language':'ru,en-us;q=0.7,en;q=0.3',
        'Cache-Control':'max-age=0',
        'Connection':'keep-alive',    
        'User-Agent':'send_sms api',
    }
    
    query_args = { 
        'guid':guid,
        'from':unicode(mfrom).encode('utf-8'),
        'priority':priority,
        'text':unicode(text).encode('utf-8')
    }
    
    encoded_args = urllib.urlencode(query_args)    
    url = API_URL+encoded_args     
    try:
        req = urllib2.Request(url, None, headers)
        html = urllib2.urlopen(req).read()    
    except:
        return ""
    return html
    
if __name__=="__main__":
    print send_signal(0,u'Привет!!!')